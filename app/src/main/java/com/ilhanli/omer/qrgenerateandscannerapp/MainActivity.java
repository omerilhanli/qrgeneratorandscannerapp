package com.ilhanli.omer.qrgenerateandscannerapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.Result;

import net.glxn.qrgen.android.QRCode;
import net.glxn.qrgen.core.image.ImageType;

import java.io.File;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mScannerView = findViewById(R.id.zxing_scanner_view);

        webView = findViewById(R.id.web_view);
    }

    public void scanQrCode(View view) {

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.

        mScannerView.startCamera();
    }

    @Override
    public void onPause() {

        super.onPause();

        mScannerView.stopCamera();   // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {

        String txt = result.getText();

        if (!txt.isEmpty()) {

            mScannerView.stopCamera();

            mScannerView.setVisibility(View.GONE);

            webView.setVisibility(View.VISIBLE);

            if (isNetworkConnected()) {

//                txt = "https://www.youtube.com/watch?v=1W5BA0lDVLM&list=RD1W5BA0lDVLM&start_radio=1";

                if (txt.contains("youtu"))

                    playYoutubeVideo(txt);

            } else {

                Toast.makeText(this, "Connection not found...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void playYoutubeVideo(String URL) {

        System.out.println("Değeri - URL:" + URL);

        if (URL.isEmpty()) {
            return;
        }

        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(URL);

        webView.setPadding(0, 0, 0, 0);

        webView.setInitialScale(getScale());

        webView.setWebViewClient(new WebViewClient());
    }

    private int getScale(){
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width)/new Double(300);
        val = val * 100d;
        return val.intValue();
    }

    private boolean isNetworkConnected() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onStop() {
        super.onStop();

        if (webView != null) {

            webView.destroy();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_qr_generator:

                startActivity(new Intent(this, CodeGeneratorActivity.class));

                return true;

            case R.id.action_exit:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    finishAndRemoveTask();

                } else {

                    finishAffinity();
                }

                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
