package com.ilhanli.omer.qrgenerateandscannerapp;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import net.glxn.qrgen.android.QRCode;
import net.glxn.qrgen.core.image.ImageType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CodeGeneratorActivity extends AppCompatActivity {

    EditText editText;

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_code_generator);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editText = findViewById(R.id.edit_text);

        imageView = findViewById(R.id.image_view);
    }

    public void convertQrCode(View view) {

        String textForQr = editText.getText().toString();

        Bitmap bitmap = getBitmapFromFile(textForQr);

        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
    }

    public Bitmap getBitmapFromFile(String text) {

        if (text.isEmpty()) {

            Toast.makeText(this, "Lütfen bir bilgi girin...", Toast.LENGTH_SHORT).show();

            return null;
        }

        File file = QRCode.from(text).to(ImageType.JPG).withSize(250, 250).file();

        String filePath = file.getPath();

        Bitmap bitmap = BitmapFactory.decodeFile(filePath);

        if (bitmap != null) {

//            saveImageToInternalStorage(bitmap);
        }

        return bitmap;
    }

    public boolean saveImageToInternalStorage2(Bitmap image) {

        try {

            // Use the compress method on the Bitmap object to write image to
            // the OutputStream
            FileOutputStream fos = openFileOutput("desiredFilename.png", Context.MODE_PRIVATE);

            // Writing the bitmap to the output stream
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);

            fos.close();

            return true;

        } catch (Exception e) {
            Log.e("saveToInternalStorage()", e.getMessage());
            return false;
        }
    }

    private void saveToInternalStorage2(Bitmap bitmapImage) {
        File mediaStorageDir = new File("/Download");

        if (!mediaStorageDir.exists()) {

            mediaStorageDir.mkdirs();
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());

        String mImageName = "QRCODE_" + timeStamp + ".jpeg";

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);

        FileOutputStream fos = null;

        try {

            fos = new FileOutputStream(mediaFile);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            try {

                if (fos != null)
                    fos.close();

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case android.R.id.home:

                onBackPressed();

                break;

            default:
                break;
        }

        return (super.onOptionsItemSelected(menuItem));
    }
}
